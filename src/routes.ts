import Login from './modules/Login'
import Password from './modules/ForgotPassword'
import Reset from './modules/ResetPassword'

type Route = {
  path: string,
  exact? : boolean,
  name?: string,
  component: any
}

export type Routes = Route[];

const routes: Routes = [
  {
    path: "/",
    exact: true,
    component: Login 
  },
  {
    path: '/forgot-password',
    component: Password
  },
  {
    path: '/reset-password',
    component: Reset
  }
]

export default routes