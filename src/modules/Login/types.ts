import * as c from './constants';

export type State = {
  fail: string | null,
  loading: boolean,
  token: string
}

export type Reducer = (
  state: State,
  payload: {
    type:
    | typeof c.FETCH_LOGIN
    | typeof c.FETCH_LOGIN_SUCCESS
    | typeof c.FETCH_LOGIN_FAIL
    payload: any
  }
) => State