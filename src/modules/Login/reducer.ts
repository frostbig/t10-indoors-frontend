import * as c from './constants';
import produce from 'immer';
import { Reducer, State } from './types';

const INITIAL_STATE: State = {
  fail: null,
  loading: false,
  token: ''
}

const login: Reducer = (state = INITIAL_STATE, { type, payload }) =>
  produce(state, draft => {
    switch (type) {
      case c.FETCH_LOGIN:
        draft.loading = true
        return;

      case c.FETCH_LOGIN_SUCCESS:
        draft.loading = false;
        if (payload.response) {
          draft.token = payload.data
          const token = payload.response.data.response.token
          localStorage.setItem('token', JSON.stringify(token))
          window.location.href = '/user'
        } else {
          draft.fail = 'Ocorreu um erro no servidor';
        }
        return;

      case c.FETCH_LOGIN_FAIL:
        draft.loading = false;
        draft.fail = payload.error;
        return;
    }
  })

export default login;