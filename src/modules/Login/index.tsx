import React, { SyntheticEvent } from 'react';
import Input from '../../components/Input';
import Button from '../../components/Button';
import Header from '../../components/Header/index';
import { fetchLogin } from './actions';
import { reducers } from '../../store/reducers';
import './style.css';
import { useDispatch, useSelector } from 'react-redux';
import createToaster from '../../components/Toaster';
import { getValuesFromForm } from '../../utils/form';

const Login: React.FC = () => {
  const dispatch = useDispatch();

  const { fail, loading } = useSelector((state: typeof reducers) => state.login)
  const showToaster = createToaster({
    message: 'Usuário ou senha incorretos',
    intent: 'danger',
    icon: 'warning-sign',
  })

  const sendLogin = (e: SyntheticEvent) => {
    e.preventDefault()
    if (e.target instanceof HTMLFormElement) {
      const inputValues = getValuesFromForm(e.target);
      dispatch(fetchLogin({
        email: inputValues[0],
        password: inputValues[1]
      }))
    }
  }

  return (
    <>
    <Header />
    {fail ? showToaster() : null}
      <div className='container'>
        <h2>Conclua o login para acessar a plataforma</h2>
        <form onSubmit={sendLogin}>
          <Input placeholder='Email' type='email' />
          <Input placeholder='Senha' type='password' />
          <div className='submit'> 
            <Button name='primary' loading={loading}>
              Entrar
            </Button>
          </div>
          <Button type='submit' href='/forgot-password' name='secondary'>
            esqueceu a senha?
          </Button>
        </form>
      </div>
    </>
  );
}

export default Login