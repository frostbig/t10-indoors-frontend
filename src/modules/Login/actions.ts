import * as c from './constants';
import { Dispatch } from 'redux';

export const fetchLogin = (
  payload: { email: string, password: string }
) => (dispatch: Dispatch) => {
  dispatch({
    type: c.FETCH_LOGIN,
    payload: {
      request: {
        url: '/auth',
        method: 'post',
        data: payload
      },
      options: {
        onSuccess: ({ getState, dispatch, response }: any) =>
          dispatch({
            type: c.FETCH_LOGIN_SUCCESS,
            payload: {
              response: response
            }
          }),
        onError: ({ getState, dispatch, error }: any) =>
          dispatch({
            type: c.FETCH_LOGIN_FAIL,
            payload: {
              error
            }
          })
      }
    }
  });
};



