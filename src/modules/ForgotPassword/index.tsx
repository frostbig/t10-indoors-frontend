import React, { SyntheticEvent } from 'react';
import Header from '../../components/Header'
import Input from '../../components/Input';
import Button from '../../components/Button'
import sendForgot from './actions'
import './style.css';
import { getValuesFromForm } from '../../utils/form';
import { reducers } from '../../store/reducers';
import { useDispatch, useSelector } from 'react-redux';
import createToaster from '../../components/Toaster';

const Password: React.FC = () => {
  const dispatch = useDispatch();
  const showToaster = createToaster({
    message: 'O email inserido não está cadastrado!',
    intent: 'danger',
    icon: 'warning-sign',

  })
  const { fail, loading} = useSelector((state: typeof reducers) => state.forgotpassword)

  const sendEmail = (e: SyntheticEvent) => {
    e.preventDefault();
    if(e.target instanceof HTMLFormElement){
      const inputValues = getValuesFromForm(e.target);
      dispatch(sendForgot({
        email: inputValues[0]
      }))
    }
  }
  
  return(
  <>
<Header />
{fail !== null ? showToaster() : null}
<div className='container'>
  <h2> Informe seu email para a recuperação de sua senha</h2>
  <form onSubmit={sendEmail}>
  <Input placeholder='Email' type='email'/>
  <Button name='primary' loading={loading}>
  Redefinir senha
  </Button>
  <Button name='warning' href='login'>
    Cancelar
  </Button>
  </form>

</div>
  </>
)
}

export default Password;