import * as c from './constants';
import { Dispatch } from 'redux';

const fetchForgot = (
  payload: { email: string }
) => (dispatch: Dispatch) => {
  dispatch({
    type: c.FETCH_FORGOT,
    payload: {
      request: {
        url: '/forgot-password',
        method: 'post',
        data: payload
      },
      options: {
        onSuccess: ({ getState, dispatch, response }: any) =>
          dispatch({
            type: c.FETCH_FORGOT_SUCCESS,
            payload: {
              response: response
            }
          }),
        onError: ({ getState, dispatch, error }: any) =>
          dispatch({
            type: c.FETCH_FORGOT_FAIL,
            payload: {
              error
            }
          })
      }
    }
  });
};

export default fetchForgot