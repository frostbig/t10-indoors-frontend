import * as c from './constants';
import produce from 'immer';
import { Reducer, State } from './types';

const INITIAL_STATE: State = {
  fail: null,
  loading: false,
  tokenReset: ''
}

const forgot:Reducer = (state = INITIAL_STATE, { type, payload }) => 
  produce(state, draft => {
    switch (type) {
      case c.FETCH_FORGOT:
        draft.loading = true
        return;

      case c.FETCH_FORGOT_SUCCESS:
        draft.loading = false
        if (payload.response) {
          draft.tokenReset = payload.data
          const token = payload.response.data.response.tokenReset
          localStorage.setItem("tokenReset", JSON.stringify(token))
        } else {
          draft.fail = 'Ocorreu um erro no servidor'
        }
        return;

      case c.FETCH_FORGOT_FAIL:
        draft.loading = false;
        draft.fail = payload.error;
        return;
    }
  })

export default forgot;