import * as c from './constants';

export type State = {
  fail: string | null,
  loading: boolean,
  tokenReset: string
}

export type Reducer = (
  state: State,
  payload: {
    type:
    | typeof c.FETCH_FORGOT
    | typeof c.FETCH_FORGOT_SUCCESS
    | typeof c.FETCH_FORGOT_FAIL
    payload: any
  }
) => State