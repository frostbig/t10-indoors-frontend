import * as c from './constants';
import { Dispatch } from 'redux';

const fetchReset = (
  payload: { token: string, tokenReset: string, password: string }
) => (dispatch: Dispatch) => {
  dispatch({
    type: c.FETCH_RESET,
    payload: {
      request: {
        url: '/reset-password',
        method: 'post',
        data: payload
      },
        options: {
          onSuccess: ({ getState, dispatch, response }: any) =>
            dispatch({
              type: c.FETCH_RESET_SUCCESS,
              payload: {
                response
              }
            }),

          onError: ({ getState, dispatch, error }: any) =>
            dispatch({
              type: c.FETCH_RESET_FAIL,
              payload: {
                error
              }
            })
      }
    }
  })
}

export default fetchReset