import React, { SyntheticEvent } from 'react';
import Header from '../../components/Header';
import Button from '../../components/Button';
import Input from '../../components/Input';
import './style.css';
import { getValuesFromForm } from '../../utils/form';
import { reducers } from '../../store/reducers';
import fetchReset from './action';
import { useDispatch, useSelector } from 'react-redux';
import createToaster from '../../components/Toaster';

const Reset: React.FC = () => {
  const dispatch = useDispatch()
  const showToasterPass = createToaster({
    message: 'As senhas não coincidem',
    icon: 'warning-sign',
    intent: 'danger'
  })
  const showToaster = createToaster({
    message: 'token inválido',
    icon: 'warning-sign',
    intent: 'danger'
  })
  const { fail, loading } = useSelector((state: typeof reducers) => state.resetpassword)

  const sendReset = (e: SyntheticEvent) => {
    e.preventDefault();
    const tokenReset = localStorage.getItem('tokenReset');
    if (e.target instanceof HTMLFormElement) {
      const inputValues = getValuesFromForm(e.target)
      if (inputValues[0] !== inputValues[1]) {
        showToasterPass()
        return;
      }
      dispatch(fetchReset({
        token: inputValues[2],
        tokenReset: tokenReset || '',
        password: inputValues[1]
      }))
    }
  }

  return (
    <>
      <Header />
      {fail ? showToaster(): null}
      <div className='container'>
        <h2>Digite sua nova senha</h2>
        <form onSubmit={sendReset}>
          <Input placeholder='Nova senha' type='text' />
          <Input placeholder='Confirmação da senha' type='text' />
          <Input placeholder='Token' type='text' />
          <Button name='primary' loading={loading} >
            Redefinir senha
    </Button>
        </form>
      </div>
    </>
  )
}

export default Reset;