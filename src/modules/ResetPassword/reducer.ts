import * as c from './constants';
import produce from 'immer';
import { Reducer, State } from './types';

const INITIAL_STATE: State = {
  fail: null,
  loading: false
}

const reset: Reducer = (state = INITIAL_STATE, { type, payload }) => (
  produce(state, draft => {
    console.log(type, payload)
    switch (type) {
      case c.FETCH_RESET:
        draft.loading = true
        return;

      case c.FETCH_RESET_SUCCESS:
        draft.loading = false
        if (payload.data) {
          window.location.href = '/'
        } else {
          draft.fail = 'Ocorreu um erro no servidor'
        }
        return;

      case c.FETCH_RESET_FAIL:
        draft.loading = false;
        draft.fail = payload.error;
        return;
    }
  })
)

export default reset