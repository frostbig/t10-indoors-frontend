import * as c from './constants';

export type State = {
  fail: string | null,
  loading: boolean,
}

export type Reducer = (
  state: State,
  payload: {
    type:
    | typeof c.FETCH_RESET
    | typeof c.FETCH_RESET_SUCCESS
    | typeof c.FETCH_RESET_FAIL

    payload: any
  }
) => State