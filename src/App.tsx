import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import {Provider} from 'react-redux';
import store from './store';
import routes from './routes';

const App: React.FC = () => {
  return (
    <Provider store={store}>
    <BrowserRouter>
      <Switch>
        {routes.map(r => (
          <Route key={r.path} {...r} />
        ))}
      </Switch>
    </BrowserRouter>
    </Provider>
  );
}

export default App;
