import React from 'react';
import { Button as BButon, IButtonProps } from '@blueprintjs/core';
import './style.css';

interface IButton extends IButtonProps {
  name: string,
  href?: string
};

const Button: React.FC<IButton> = (props) => {
  const { href, children, name, type } = props;
  if(href && name !== 'primary'){
    return (
      <a className={`button ${name}`} href={href}>
        {children}
      </a>
    )
  }

  return (
    <BButon className={`button ${name} `} type={type} {...props}>
      {children}
    </BButon>
  )
};

export default Button;