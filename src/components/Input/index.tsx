import React from 'react';
import {InputGroup, IInputGroupProps} from '@blueprintjs/core';
import './style.css'

const Input: React.FC<IInputGroupProps> = (props) => {
  return(
    <InputGroup className="input" {...props} required />
  );
}
export default Input;
