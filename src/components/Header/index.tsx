import React from 'react';
import Logo from '../../assets/images/logoT10.png';
import './style.css';

const Header: React.FC = () => {
  return (
    <header className='header'>
      <a href='/'>
        <img src={Logo} alt='Logo' />
      </a>
    </header>
  )
}

export default Header;