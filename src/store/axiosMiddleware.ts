import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';

const client = axios.create({
  baseURL : process.env.REACT_APP_API,
  responseType: 'json'
});

export default axiosMiddleware(client)